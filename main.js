var Router = require("./modules/routes");
var router = new Router(8080)

router.setRoutes();

router.startServer();
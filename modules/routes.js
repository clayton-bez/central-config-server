var express = require('express');
var http = require('http');
var Database = require("./dbHandler.js");
class routes{
    constructor(port){
        this.port = port;
        this.app = express();
        this.server = http.Server(this.app);
        this.db = new Database("configure.db.json");
    }
    setRoutes(){
        var self = this;
        self.app.get("/config/:configID",(req,res)=>{
            var id = req.params.configID;
            self.db.get(`/${id}`,(err,data)=>{
                if(!err)
                    res.send(data);
                else
                    res.sendStatus(404);
            })
        });
        self.app.post("/save",function(req,res){
            var str = '';
            req.on("data",function(chunk){
                str += chunk.toString();
            });
            req.on("end",function(){
                str = JSON.parse(str);
                var id = str.configID;
                delete(str.configID);
                self.db.add(`/${id}`,str,(err)=>{
                    if(!err)
                        res.sendStatus(200);
                    else    
                        res.sendStatus(500);
                })
            });
        
        });
    }
    startServer(){
        this.server.listen(this.port,(err)=>{
            if(!err)
                console.log('Server started on port '+this.port);
            
        });
    }
}
module.exports = routes;
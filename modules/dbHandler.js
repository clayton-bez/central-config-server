var JsonDB = require('node-json-db');

class db{
    constructor(databaseName){
        this.db = new JsonDB(databaseName,true,true);
    }
    add(route,data,callback){
        try{
            this.db.push(route,data);
            callback(null,true);
        }
        catch(e){
            callback(err,null);
        }
        
    }
    get(route,callback){
        try{
            var data = this.db.getData(route);
            callback(null,data);
        }
        catch(e){
            callback(e,null);
        }

    }
}

module.exports = db;